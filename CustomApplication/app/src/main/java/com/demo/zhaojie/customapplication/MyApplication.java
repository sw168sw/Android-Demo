package com.demo.zhaojie.customapplication;

import android.app.Activity;
import android.app.Application;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by zhaojie on 2015/4/11.
 */
public class MyApplication extends Application {
    private static MyApplication instance;

    private List<Activity> activityList = new LinkedList<>();

    public static MyApplication getInstance() {
        if (null == instance) {
            instance = new MyApplication();
        }
        return instance;
    }

    public void addActivity(Activity activity) {
        activityList.add(activity);
    }

    public void exit() {
        //save cache to db
        for (Activity activity : activityList) {
            activity.finish();
        }
    }
}
